let nav=document.querySelector('.nav');
let brandName=getQueryString('brandName');
let brandTitleId=getQueryString('brandTitleId');
$('.nav>a:eq(2)').text(brandName);
// fetch('')
$('.logo').click(function(){
    history.back();
})
let container=document.querySelector('.container');
function getData(pageid = 1){fetch(`http://chst.vip:1234/api/getbrandproductlist?brandtitleid=${brandTitleId}&pageid=${pageid}`)
.then(body=>body.json())
.then(res=>{
    console.log(res);
    let arr=res.result;
    arr.forEach(item => {
        let li = document.createElement('li');
        let domStr = ` ${item.productImg}
        <div class="mes">
            <h1>${item.productName}</h1>
            <p>${item.productPrice}</p>
          <div class='info'>  
          <b>${item.productQuote}</b>
          <i>${item.productCom}</i></div>
          <em name='${item.brandName}'>${item.productId}</em>
        </div>`;
        li.innerHTML = domStr;
        container.appendChild(li);
        $('li').click(function () {
            let id=$(this).find('em').text();
            let name=$(this).find('em').attr('name')
            // id=decodeURI(id);
            location.href = './productlist.html?id='+id+'&name='+name+'&category='+brandName+'&brandTitleId='+brandTitleId;
            // console.log($(this))
        })
    });
})
}
getData();
let pageid = 1;
lazyLoad(function () {
    pageid++;
    if (pageid > 10) {
       return
    }
    getData(pageid);
}, 100)