let container = document.querySelector('.container');
function getData(pageid = 1) {
    fetch(`http://chst.vip:1234/api/getmoneyctrl?pageid=${pageid}`)
        .then(body => body.json())
        .then(res => {
            console.log(res)
            render(res.result, container)
        })
}

getData();
function render(data, el) {
    data.forEach(item => {
        let regExp = /(?<=imgurl=)[^'"]*/
        let result = item.productImg2.match(regExp);
        console.log(result)
        let li = document.createElement('li');
        let domStr = ` <img src=${result[0]} alt="" width="100px">
        <div class="mes">
            <h1>${item.productName}<span>${item.productPinkage}</span></h1>
            <div class="m">
                <h2>
                    <span>${item.productFrom}</span>
                    <i>${item.productTime}</i>
                </h2>
                <p>${item.productComCount}</p>
            </div>
            <b>${item.productId}</b>
        </div>`;
        li.innerHTML = domStr;
        el.appendChild(li);
        $('li').click(function () {
            let id=$(this).find('b').text();
            // id=decodeURI(id);
            location.href = './details.html?id='+id;
            // console.log($(this))
        })
    });
}
let pageid = 1;
lazyLoad(function () {
    pageid++;
    if (pageid > 14) {
       return
    }
    getData(pageid);
}, 100)