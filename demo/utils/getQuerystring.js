function getQueryString(params) {
    let addr = location.href;
    console.log(addr)
    let reg = /(?<=\?).*/;
    let r = addr.match(reg);
    if (!r) {
        console.error('没有匹配到参数');
        return
    } else {
        r = decodeURI(r[0]);
        if (params) {
            if (r.indexOf(params) == -1) {
                return null;
            }
            let arr = r.split('&');
            // console.log(arr)
            let obj = {};
            arr.forEach(item => {
                let itemArr = item.split('=');
                // console.log(itemArr)
                obj[itemArr[0]] = itemArr[1]

            });
            return obj[params];
        } else {
            console.error("未传递参数");
            return
        }
    }
}