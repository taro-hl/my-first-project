function lazyLoad(fn,minScollHeight=100){
    let flag=true;
    window.onscroll=function(){
        let scrollTop=document.documentElement.scrollTop;
        let clientHeight=document.documentElement.clientHeight;
        let scrollHeight=document.documentElement.scrollHeight;
        let scrollBottom=scrollHeight-clientHeight-scrollTop;
        if(scrollBottom<=minScollHeight){
            if(flag){
                flag=false;
                fn();
            }else{
                flag=true;
            }
        }
    }
}